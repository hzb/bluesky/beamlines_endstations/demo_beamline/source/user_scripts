# start of automatically prepended lines
from beamlinetools.BEAMLINE_CONFIG import *
# ipython = get_ipython()


# To run this script just type load_user_script('examples/example_with_functions.py')
# Now the function test_plan() 
# is available in the ipython session, so just type test_plan() to execute it


def test_plan():
    # print the pocurrent energy of the monochromator
    print(f"Current position of the PGM {pgm.en.get()}")

    # move the energy to 403 eV
    run_plan("%mov pgm.en 403")

    # run a scan
    run_plan("%dscan [noisy_det] motor -1 1 10")